Shader "Unlit/TransparentEdgeShaderPolygon"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _BarycentricCoordinates ("BarycentricCoordinates", Vector) = (0.5, 0.5, 0.0)
        _Width ("Width", float) = 1
        _Height ("Height", float) = 1
    }
    SubShader
    {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            Cull Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                fixed4 localCoords : LOCAL_COORDS;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            vector _BarycentricCoordinates;
            float _Width;
            float _Height;

            v2f vert (appdata v)
            {
                v2f o;
                o.localCoords = v.vertex;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                //col[0] = i.localPosition[0];
                //col[1] = i.localPosition[1];
                //col[2] = i.localPosition[2];
                //col[3] = i.localPosition[3];
                float2 vertexNorm;
                vertexNorm[0] = i.vertex[0] / _Width;
                vertexNorm[1] = i.vertex[1] / _Height;
                float widthNorm = i.localCoords[2] / (_Width / 2);
                float heightNorm = i.localCoords[0] / (_Height / 2);
                float1 distWidth = distance(widthNorm, _BarycentricCoordinates[2]);
                float1 distHeight = distance(heightNorm, _BarycentricCoordinates[3]);
                float alpha = 1 -smoothstep(0.3, 0.5, sqrt(distWidth * distWidth + distHeight * distHeight));
                //col[0] = distWidth;
                //col[1] = 0;
                //col[2] = distHeight;
                col[3] = alpha;
                /*
                col[0] = i.localCoords[0] / (_Width / 2);
                col[1] = i.localCoords[0] / (_Width / 2);
                col[2] = i.localCoords[0] / (_Width / 2);

                col[0] = i.localCoords[2] / (_Height / 2);
                col[1] = i.localCoords[2] / (_Height / 2);
                col[2] = i.localCoords[2] / (_Height / 2);
                */
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}

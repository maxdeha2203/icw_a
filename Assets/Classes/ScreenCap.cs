using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCap
{
    Vector3 _cameraPosition;
    Quaternion _cameraRotation;
    Texture2D _texture2D;
    Texture2D _croppedTexture2D;
    double _distance;
    float _fov;
    double _aspect;

    public ScreenCap(
        Vector3 cameraPosition,
        Quaternion cameraRotation,
        Texture2D texture2D,
        double distance,
        float fov,
        double aspect)
    {
        _cameraPosition = cameraPosition;
        _cameraRotation = cameraRotation;
        _texture2D = texture2D;
        _distance = distance;
        _fov = fov;
        _aspect = aspect;
    }

    public Vector3 CameraPosition
    {
        get => _cameraPosition;
        set => _cameraPosition = value;
    }

    public Quaternion CameraRotation
    {
        get => _cameraRotation;
        set => _cameraRotation = value;
    }

    public Texture2D Texture2D
    {
        get => _texture2D;
        set => _texture2D = value;
    }
    
    public Texture2D CroppedTexture2D
    {
        get => _croppedTexture2D;
        set => _croppedTexture2D = value;
    }
    
    public double Distance
    {
        get => _distance;
        set => _distance = value;
    }
    
    public float Fov
    {
        get => _fov;
        set => _fov = value;
    }
    
    public double Aspect
    {
        get => _aspect;
        set => _aspect = value;
    }
}

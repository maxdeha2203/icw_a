using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{
    [SerializeField]
    List<ScreenController> screens;

    static ScreenManager _instance;
    
    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;
        
        foreach (var screen in screens)
        {
            if (screen.GetScreenType() == ScreenType.MainScreen) screen.gameObject.SetActive(true);
            else screen.gameObject.SetActive(false);
        }
    }

    public static ScreenManager GetInstance()
    {
        return _instance;
    }

    public void SwitchScreen(ScreenType targetType)
    {
        foreach (var screen in screens)
        {
            if (screen.GetScreenType() == targetType) screen.gameObject.SetActive(true);
            else screen.gameObject.SetActive(false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ScreenType
{
    MainScreen,
    EntireScreen,
    AABB,
    Ar2dImage,
    Polygon,
}

public abstract class ScreenController : MonoBehaviour
{
    [SerializeField]
    ScreenType screenType;

    public ScreenType GetScreenType()
    {
        return screenType;
    }
}

using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SwitchToScreen : MonoBehaviour
{
    [SerializeField]
    ScreenType target;

    void Start()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        ScreenManager.GetInstance().SwitchScreen(target);
    }
}

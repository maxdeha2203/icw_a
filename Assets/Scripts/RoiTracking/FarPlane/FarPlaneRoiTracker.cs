using System;
using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FarPlaneRoiTracker : MonoBehaviour
{
    [SerializeField]
    GameObject planePrefab;
    
    [SerializeField]
    GameObject polygonPlanePrefab;
    
    [SerializeField]
    GameObject transparentPlanePrefab;
    
    [SerializeField]
    GameObject arContentHolder;

    [SerializeField]
    Camera dummyCam;

    GameObject transparentPlane;
    
    public void StartTrackingEntireScreen()
    {
        StartCoroutine(TrackEntireScreen());
    }

    IEnumerator TrackEntireScreen()
    {
        yield return StartCoroutine(ScreenCapUtil.GetInstance().CreateScreenCap());

        while (ScreenCapUtil.GetInstance().IsScreenCapInProgress())
        { 
            yield return null;
        }
        
        ScreenCap screenCap = ScreenCapUtil.GetInstance().GetScreenCap();
        InstantiateEntireScreenPlane(screenCap);
    }

    void InstantiateEntireScreenPlane(ScreenCap screenCap)
    {
        double planeDistance = screenCap.Distance;
        double angle = screenCap.Fov / 2;
        double planeHeight = 2 * Math.Sin(Mathf.Deg2Rad * angle) * (planeDistance / Math.Cos(Mathf.Deg2Rad * angle));
        double planeWidth = screenCap.Aspect * planeHeight;

        Vector3 direction = screenCap.CameraRotation * Vector3.forward;
        Vector3 position = (direction * (float)planeDistance) + screenCap.CameraPosition;

        GameObject plane = Instantiate(planePrefab, arContentHolder.transform);
        
        plane.transform.localScale = new Vector3((float)planeWidth, (float)planeHeight, 1f);
        plane.transform.position = position;
        plane.transform.LookAt(screenCap.CameraPosition);
        plane.GetComponent<PlanePrefabController>().SetPlaneTexture(screenCap.Texture2D);
    }

    public void InstantiateCroppedPlane(ScreenCap screenCap, Texture2D croppedTex, float[] centerPointUv)
    {
        double planeDistance = screenCap.Distance;
        double angle = screenCap.Fov / 2;
        double totalPlaneHeight = 2 * Math.Sin(Mathf.Deg2Rad * angle) * (planeDistance / Math.Cos(Mathf.Deg2Rad * angle));
        double totalPlaneWidth = screenCap.Aspect * totalPlaneHeight;

        double scaleWidth = croppedTex.width / (double)screenCap.Texture2D.width;
        double scaleHeight = croppedTex.height / (double)screenCap.Texture2D.height;

        //scale to match cropped size!
        double planeWidth = totalPlaneWidth * scaleWidth;
        double planeHeight = totalPlaneHeight * scaleHeight;
        
        Vector3 direction = screenCap.CameraRotation * Vector3.forward;
        Vector3 position = (direction * (float)planeDistance) + screenCap.CameraPosition;
        
        GameObject plane = Instantiate(planePrefab, arContentHolder.transform);
        Transform planeTransform = plane.transform;
        planeTransform.localScale = new Vector3((float)planeWidth, (float)planeHeight, 1f);
        planeTransform.position = position;
        planeTransform.rotation = screenCap.CameraRotation;
        
        //reposition to match centerpoint of cropped area!
        Vector3 pos = RaycastBackgroundPlane(screenCap, centerPointUv);
        plane.transform.position = new Vector3(pos.x, pos.y, pos.z);

        plane.GetComponent<PlanePrefabController>().SetPlaneTexture(croppedTex);
    }

    public void InstantiateCroppedPolygonPlane(ScreenCap screenCap, Texture2D croppedTex, List<Vector3> cornerCoords, List<Vector2> uvCoords)
    {
        double planeDistance = screenCap.Distance;
        double angle = screenCap.Fov / 2;
        double totalPlaneHeight = 2 * Math.Sin(Mathf.Deg2Rad * angle) * (planeDistance / Math.Cos(Mathf.Deg2Rad * angle));
        double totalPlaneWidth = screenCap.Aspect * totalPlaneHeight;

        double scaleWidth = (double)croppedTex.width / (double)screenCap.Texture2D.width;
        double scaleHeight = (double)croppedTex.height / (double)screenCap.Texture2D.height;

        //scale to match cropped size!
        double planeWidth = totalPlaneWidth * scaleWidth;
        double planeHeight = totalPlaneHeight * scaleHeight;
        
        Vector3 direction = screenCap.CameraRotation * Vector3.forward;
        Vector3 position = (direction * (float)planeDistance) + screenCap.CameraPosition;
        
        GameObject plane = Instantiate(polygonPlanePrefab);
        plane.GetComponent<PolygonPlanePrefabController>().CreateMesh(cornerCoords.ToArray(), uvCoords.ToArray());
        plane.transform.parent = transparentPlane.transform.GetChild(0).transform;
        plane.transform.localPosition = Vector3.zero;
        plane.transform.localRotation = Quaternion.identity;
        plane.transform.localScale = Vector3.one;

    }
    
    public void StartCreateRaycastableBackground()
    {
        StartCoroutine(CreateRaycastableBackground());
    }
    
    IEnumerator CreateRaycastableBackground()
    {
        yield return StartCoroutine(ScreenCapUtil.GetInstance().CreateScreenCap());

        while (ScreenCapUtil.GetInstance().IsScreenCapInProgress())
        { 
            yield return null;
        }
        
        ScreenCap screenCap = ScreenCapUtil.GetInstance().GetScreenCap();
        InstantiateTransparentBackgroundPlane(screenCap);
    }

    void InstantiateTransparentBackgroundPlane(ScreenCap screenCap)
    {
        dummyCam.transform.rotation = screenCap.CameraRotation;
        dummyCam.transform.position = screenCap.CameraPosition;
        
        double planeDistance = screenCap.Distance;
        double angle = screenCap.Fov / 2;
        double planeHeight = 2 * Math.Sin(Mathf.Deg2Rad * angle) * (planeDistance / Math.Cos(Mathf.Deg2Rad * angle));
        double planeWidth = screenCap.Aspect * planeHeight;

        Vector3 direction = screenCap.CameraRotation * Vector3.forward;
        Vector3 position = (direction * (float)planeDistance) + screenCap.CameraPosition;
        
        GameObject plane = Instantiate(transparentPlanePrefab, arContentHolder.transform);
        transparentPlane = plane;
        
        plane.transform.localScale = new Vector3((float)planeWidth, (float)planeHeight, 1f);
        plane.transform.position = position;
        plane.transform.rotation = screenCap.CameraRotation;
    }

    Vector3 RaycastBackgroundPlane(ScreenCap screenCap, float[] centerPointUv)
    {
        dummyCam.transform.position = screenCap.CameraPosition;
        dummyCam.transform.rotation = screenCap.CameraRotation;
        dummyCam.fieldOfView = screenCap.Fov;
        Vector3 result = Vector3.zero;
        TransparentPlaneRaycastingUtil.GetInstance().RaycastTransparentPlane(centerPointUv, LayerMaskName.TransparentPlane, out result);
        return result;
    }

    public GameObject GetTransparentPlane()
    {
        return transparentPlane;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public enum AABBProgressState
{
    Init,
    CaptureScreenshot,
    AABBDrawing,
    Finished,
}

public class AABBTrackingManager : MonoBehaviour
{
    [SerializeField]
    RawImage screenshotRawImage;

    [SerializeField]
    GameObject captureScreenButton;

    [SerializeField]
    AABBDrawingUtil aabbDrawingUtil;
    
    [SerializeField]
    FarPlaneRoiTracker farPlaneRoiTracker;

    [SerializeField]
    GameObject panel;

    static AABBTrackingManager _instance;

    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;
    }

    public static AABBTrackingManager GetInstance()
    {
        return _instance;
    }
    

    public void SetAABBProgress(AABBProgressState newState)
    {
        switch (newState)
        {
            case AABBProgressState.CaptureScreenshot:
                Debug.Log("Switching to Capture Screenshot State");
                StartCoroutine(GetScreenshot());
                break;
            case AABBProgressState.AABBDrawing:
                Debug.Log("Switching to AABB Drawing State");
                farPlaneRoiTracker.StartCreateRaycastableBackground();
                ShowScreenshot();
                aabbDrawingUtil.gameObject.SetActive(true);
                panel.SetActive(true);
                aabbDrawingUtil.SetPanelRect(panel);
                break;
            case AABBProgressState.Finished:
                Debug.Log("Switching to Finished State");
                aabbDrawingUtil.SetRectUvCoords();
                CropScreenshot();
                screenshotRawImage.gameObject.SetActive(false);
                captureScreenButton.gameObject.SetActive(false);
                panel.SetActive(false);
                break;
        }
    }

    IEnumerator GetScreenshot()
    {
        yield return StartCoroutine(ScreenCapUtil.GetInstance().CreateScreenCap());

        while (ScreenCapUtil.GetInstance().IsScreenCapInProgress())
        { 
            yield return null;
        }
        
        SetAABBProgress(AABBProgressState.AABBDrawing);
    }

    void ShowScreenshot()
    {
        screenshotRawImage.gameObject.SetActive(true);
        screenshotRawImage.material.mainTexture = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D;
        aabbDrawingUtil.gameObject.SetActive(true);
    }

    void CropScreenshot()
    {
        Texture2D oTex = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D;
        float[,] uvs = aabbDrawingUtil.GetUvRectCoords();
        int x1, x2, y1, y2;
        x1 = (int)(oTex.width * uvs[0, 0]);
        y1 = (int)(oTex.height * uvs[0, 1]);
        x2 = (int)(oTex.width * uvs[1, 0]);
        y2 = (int)(oTex.height * uvs[1, 1]);
        int width = x2 - x1;
        int height = y2 - y1;
        Color[] pixels = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D.GetPixels(x1, y1, width, height);
        Texture2D cTex = new Texture2D(width, height);
        cTex.SetPixels(pixels);
        cTex.Apply();
        float[] centerPointUv = { ((uvs[0, 0] + uvs[1, 0]) / 2), ((uvs[0, 1] + uvs[1, 1]) / 2) };
        farPlaneRoiTracker.InstantiateCroppedPlane(ScreenCapUtil.GetInstance().GetScreenCap(), cTex, centerPointUv);
    }
}


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public enum PolygonProgressState
{
    Init,
    CaptureScreenshot,
    AABBDrawing,
    Finished,
}

public class PolygonTrackingManager : MonoBehaviour
{
    [SerializeField]
    RawImage screenshotRawImage;

    [SerializeField]
    GameObject captureScreenButton;

    [SerializeField]
    PolyDrawingUtil polyDrawingUtil;
    
    [SerializeField]
    FarPlaneRoiTracker farPlaneRoiTracker;

    [SerializeField]
    GameObject panel;

    Texture2D croppedTexture;

    static PolygonTrackingManager _instance;

    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;
    }

    public static PolygonTrackingManager GetInstance()
    {
        return _instance;
    }

    public void SetPolygonProgress(PolygonProgressState newState)
    {
        switch (newState)
        {
            case PolygonProgressState.CaptureScreenshot:
                Debug.Log("Switching to Capture Screenshot State");
                StartCoroutine(GetScreenshot());
                break;
            case PolygonProgressState.AABBDrawing:
                Debug.Log("Switching to AABB Drawing State");
                farPlaneRoiTracker.StartCreateRaycastableBackground();
                ShowScreenshot();
                polyDrawingUtil.gameObject.SetActive(true);
                panel.SetActive(true);
                polyDrawingUtil.SetPanelRect(panel);
                break;
            case PolygonProgressState.Finished:
                Debug.Log("Switching to Finished State");
                float[] aabbCoords = polyDrawingUtil.GetAabbCoords();
                InstantiatePolygon(aabbCoords);
                screenshotRawImage.gameObject.SetActive(false);
                captureScreenButton.gameObject.SetActive(false);
                panel.SetActive(false);
                break;
        }
    }

    IEnumerator GetScreenshot()
    {
        yield return StartCoroutine(ScreenCapUtil.GetInstance().CreateScreenCap());

        while (ScreenCapUtil.GetInstance().IsScreenCapInProgress())
        { 
            yield return null;
        }
        
        SetPolygonProgress(PolygonProgressState.AABBDrawing);
    }

    void ShowScreenshot()
    {
        screenshotRawImage.gameObject.SetActive(true);
        screenshotRawImage.material.mainTexture = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D;
        polyDrawingUtil.gameObject.SetActive(true);
    }

    void InstantiatePolygon(float[] aabbCoords)
    {
        //crop texture
        Texture2D oTex = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D;
        int x1, x2, y1, y2;
        x1 = (int)aabbCoords[0];
        y1 = (int)aabbCoords[1];
        x2 = (int)aabbCoords[2];
        y2 = (int)aabbCoords[3];
        int width = x2 - x1;
        int height = y2 - y1;
        Color[] pixels = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D.GetPixels(x1, y1, width, height);
        Texture2D cTex = new Texture2D(width, height);
        cTex.SetPixels(pixels);
        cTex.Apply();
        croppedTexture = cTex;
        
        //instantiate poly
        List<Vector3> cornerCoords =  polyDrawingUtil.GetCornerCoordsLocalSpace();
        List<Vector2> uvCoords = polyDrawingUtil.GetCornerUvCoords(aabbCoords);
        farPlaneRoiTracker.InstantiateCroppedPolygonPlane(ScreenCapUtil.GetInstance().GetScreenCap(), croppedTexture, cornerCoords, uvCoords);
    }
}


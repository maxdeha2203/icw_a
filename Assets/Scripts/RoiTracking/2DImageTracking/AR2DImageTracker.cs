using System;
using System.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class AR2DImageTracker : MonoBehaviour
{
    [SerializeField]
    ARTrackedImageManager trackedImageManager;

    [SerializeField]
    GameObject trackedImagePrefab;

    XRReferenceImageLibrary _runtimeImageLibrary;

    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            trackedImageManager.referenceLibrary = trackedImageManager.CreateRuntimeLibrary(_runtimeImageLibrary);
            trackedImageManager.requestedMaxNumberOfMovingImages = 5;
            trackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
            trackedImageManager.enabled = true;
        }
    }

    void Update()
    {
        string debugString = "";
        debugString += $"There are {trackedImageManager.trackables.count} images being tracked. \n";

        foreach (var trackedImage in trackedImageManager.trackables)
        {
            debugString += $"Image: {trackedImage.referenceImage.name} is at " + $"{trackedImage.transform.position}" +  "with tracking state" + $"{trackedImage.trackingState}" + "\n";
        }
    }

    public IEnumerator InitImageJob(Texture2D texture)
    {
        yield return null;
        
        MutableRuntimeReferenceImageLibrary mrril = trackedImageManager.referenceLibrary as MutableRuntimeReferenceImageLibrary;
        var jobHandle = mrril.ScheduleAddImageWithValidationJob(texture, Guid.NewGuid().ToString(), 0.1f);

        yield return new WaitUntil(() => jobHandle.status.IsComplete());
    }

    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs args)
    {
        foreach (ARTrackedImage trackedImage in args.added)
        {
            GameObject content = Instantiate(trackedImagePrefab, trackedImage.transform);
            content.transform.localPosition = Vector3.zero;
            content.transform.localRotation = Quaternion.identity;
        }
        
        foreach (ARTrackedImage trackedImage in args.updated)
        {
            var planeParentGo = trackedImage.transform.GetChild(0).gameObject;
            var planeGo = planeParentGo.transform.GetChild(0).gameObject;

            // Disable the visual plane if it is not being tracked
            if (trackedImage.trackingState != TrackingState.None)
            {
                planeGo.SetActive(true);

                // The image extents is only valid when the image is being tracked
                trackedImage.transform.localScale = new Vector3(trackedImage.size.x, 1f, trackedImage.size.y);

                // Set the texture
                var material = planeGo.GetComponentInChildren<MeshRenderer>().material;
                material.mainTexture = (trackedImage.referenceImage.texture == null) ? Texture2D.blackTexture : trackedImage.referenceImage.texture;
            }
            else
            {
                planeGo.SetActive(false);
            }
        }
    }
}

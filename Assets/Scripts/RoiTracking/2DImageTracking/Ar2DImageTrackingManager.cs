using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Ar2dImageTrackingProgressState
{
    Init,
    CaptureScreenshot,
    AABBDrawing,
    Finished,
}

public class Ar2DImageTrackingManager : MonoBehaviour
{
    [SerializeField]
    RawImage screenshotRawImage;

    [SerializeField]
    GameObject captureScreenButton;

    [SerializeField]
    AABBDrawingUtil aabbDrawingUtil;
    
    [SerializeField]
    AR2DImageTracker ar2DImageTracker;

    [SerializeField]
    GameObject panel;
    
    static Ar2DImageTrackingManager _instance;
    
    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;
    }

    public static Ar2DImageTrackingManager GetInstance()
    {
        return _instance;
    }
    

    public void Set2dTrackingProgress(Ar2dImageTrackingProgressState newState)
    {
        switch (newState)
        {
            case Ar2dImageTrackingProgressState.CaptureScreenshot:
                Debug.Log("Switching to Capture Screenshot State");
                StartCoroutine(GetScreenshot());
                break;
            case Ar2dImageTrackingProgressState.AABBDrawing:
                Debug.Log("Switching to AABB Drawing State");
                ShowScreenshot();
                panel.SetActive(true);
                aabbDrawingUtil.gameObject.SetActive(true);
                aabbDrawingUtil.SetPanelRect(panel);
                break;
            case Ar2dImageTrackingProgressState.Finished:
                Debug.Log("Switching to Finished State");
                aabbDrawingUtil.SetRectUvCoords();
                CropScreenshot();
                StartCoroutine(ar2DImageTracker.InitImageJob(ScreenCapUtil.GetInstance().GetScreenCap().CroppedTexture2D));
                screenshotRawImage.gameObject.SetActive(false);
                captureScreenButton.gameObject.SetActive(false);
                panel.SetActive(false);
                break;
        }
    }

    private IEnumerator GetScreenshot()
    {
        {
            yield return StartCoroutine(ScreenCapUtil.GetInstance().CreateScreenCap());
            
            while (ScreenCapUtil.GetInstance().IsScreenCapInProgress())
            { 
                yield return null;
            }
            
            Set2dTrackingProgress(Ar2dImageTrackingProgressState.AABBDrawing);
        }
    }
    
    void ShowScreenshot()
    {
        screenshotRawImage.gameObject.SetActive(true);
        screenshotRawImage.material.mainTexture = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D;
        aabbDrawingUtil.gameObject.SetActive(true);
    }
    
    void CropScreenshot()
    {
        Texture2D oTex = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D;
        float[,] uvs = aabbDrawingUtil.GetUvRectCoords();
        int x1, x2, y1, y2;
        x1 = (int)(oTex.width * uvs[0, 0]);
        y1 = (int)(oTex.height * uvs[0, 1]);
        x2 = (int)(oTex.width * uvs[1, 0]);
        y2 = (int)(oTex.height * uvs[1, 1]);
        int width = x2 - x1;
        int height = y2 - y1;
        Color[] pixels = ScreenCapUtil.GetInstance().GetScreenCap().Texture2D.GetPixels(x1, y1, width, height);
        Texture2D cTex = new Texture2D(width, height);
        cTex.SetPixels(pixels);
        cTex.Apply();
        ScreenCapUtil.GetInstance().GetScreenCap().CroppedTexture2D = cTex;
    }
}

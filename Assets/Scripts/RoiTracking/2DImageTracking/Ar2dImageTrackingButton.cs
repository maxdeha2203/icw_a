using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ar2dImageTrackingButton : MonoBehaviour
{
    [SerializeField]
    Button btn;

    [SerializeField]
    Ar2dImageTrackingProgressState nextState;

    void Start()
    {
        if (btn != null)
        {
            btn.onClick.AddListener(delegate
                {
                    Ar2DImageTrackingManager.GetInstance().Set2dTrackingProgress(nextState);
                } 
            );
        }
    }
}

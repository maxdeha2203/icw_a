using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptureScreenButtonController : MonoBehaviour
{
    [SerializeField]
    Button btn;

    [SerializeField]
    AABBProgressState nextState;
    
    void Start()
    {
        if (btn != null)
        {
            
            btn.onClick.AddListener(delegate
                {
                    Debug.Log("Switching to next state: " + nextState);
                    AABBTrackingManager.GetInstance().SetAABBProgress(nextState);
                } 
            );
        }
    }
}

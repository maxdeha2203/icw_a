using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class CaptureImageButtonController : MonoBehaviour
{
    [FormerlySerializedAs("roiTracker")]
    [SerializeField]
    FarPlaneRoiTracker farPlaneRoiTracker;
    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(farPlaneRoiTracker.StartTrackingEntireScreen);
    }
}

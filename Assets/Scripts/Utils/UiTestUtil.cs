using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UiTestUtil : MonoBehaviour
{
    static UiTestUtil _instance;
    
    GraphicRaycaster _raycaster;
    int _uiLayer;

    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;
        _uiLayer = LayerMask.NameToLayer("UI");
        _raycaster = GetComponent<GraphicRaycaster>();
    }

    public static UiTestUtil GetInstance()
    {
        return _instance;
    }

    public bool IsPointerOverUIElement(Vector2 screenCoords)
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = screenCoords;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);
        for (int index = 0; index < raycastResults.Count; index++)
        {
            if (raycastResults[index].gameObject.layer == _uiLayer) return true;
        }
        return false;
    }
}

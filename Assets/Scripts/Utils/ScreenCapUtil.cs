using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using TMPro.SpriteAssetUtilities;
using Unity.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARFoundation.VisualScripting;
using UnityEngine.XR.ARSubsystems;

public class ScreenCapUtil : MonoBehaviour
{
    [SerializeField]
    Camera arCam;

    [SerializeField]
    ARCameraManager arCamMananger;

    [SerializeField]
    RenderTexture renderTexture;

    [SerializeField]
    ARCameraBackground arCameraBackground;
    
    static ScreenCapUtil _instance;

    ScreenCap _screenCap;
    bool _isScreenCapInProgress;

    // Start is called before the first frame update
    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;
    }

    public static ScreenCapUtil GetInstance()
    {
        return _instance;
    }

    public IEnumerator CreateScreenCap()
    {
        _isScreenCapInProgress = true;

        UiController.GetInstance().SetUiVisibility(false);
        
        yield return new WaitForEndOfFrame();

        Texture2D lastCameraTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);

        if (Application.platform == RuntimePlatform.Android)
        {
            Graphics.Blit(null, renderTexture, arCameraBackground.material);
            RenderTexture activeRt = RenderTexture.active;
            RenderTexture.active = renderTexture;
            lastCameraTexture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGBA32, true);
            lastCameraTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            lastCameraTexture.Apply();
            RenderTexture.active = activeRt;
        }
        else
        {
            lastCameraTexture = ScreenCapture.CaptureScreenshotAsTexture();
        }

        //grab metadata
        Vector3 position = arCam.transform.position;
        Quaternion rotation = arCam.transform.rotation;
        double distance = arCam.farClipPlane * 0.5;
        float fov = arCam.fieldOfView;
        double aspect = arCam.aspect;

        _screenCap = new ScreenCap(position, rotation, lastCameraTexture, distance, fov, aspect);

        _isScreenCapInProgress = false;
        UiController.GetInstance().SetUiVisibility(true);
        yield return true;
    }

    public ScreenCap GetScreenCap()
    {
        return _screenCap;
    }

    public bool IsScreenCapInProgress()
    {
        return _isScreenCapInProgress;
    }
}

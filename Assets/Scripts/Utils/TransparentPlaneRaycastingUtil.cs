using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum LayerMaskName
{
    TransparentPlane,
    PolygonCorners,
}

public class TransparentPlaneRaycastingUtil : MonoBehaviour
{
    [SerializeField]
    List<LayerMaskName> layerMaskNames;

    [SerializeField]
    List<LayerMask> layerMaskValues;
    
    [SerializeField]
    Camera _dummyCam;
    
    Dictionary<LayerMaskName, LayerMask> layerMaskDict;

    static TransparentPlaneRaycastingUtil _instance;

    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;

        layerMaskDict = new Dictionary<LayerMaskName, LayerMask>();
        foreach (LayerMaskName maskName in layerMaskNames)
        {
            layerMaskDict.Add(maskName, layerMaskValues[layerMaskNames.IndexOf(maskName)]);
        }
    }

    public static TransparentPlaneRaycastingUtil GetInstance()
    {
        return _instance;
    }

    public bool RaycastTransparentPlane(float[] uvCoords, LayerMaskName layerMaskName, out Vector3 hitPos)
    {
        _dummyCam.gameObject.SetActive(true);
        
        Vector3 result = Vector3.zero;
        Vector3 screenPoint = new Vector3(uvCoords[0] * _dummyCam.pixelWidth, uvCoords[1] * _dummyCam.pixelHeight, 0);
        Ray ray = _dummyCam.ScreenPointToRay(screenPoint);
        if (layerMaskDict.TryGetValue(layerMaskName, out LayerMask layerMask))
        {
            if (Physics.Raycast(ray, out RaycastHit hitInfo, 999999, layerMask))
            {
                result = hitInfo.point;
                hitPos = result;
                _dummyCam.gameObject.SetActive(false);
                return true;
            }
        }
        
        _dummyCam.gameObject.SetActive(false);
        hitPos = Vector3.zero;
        return false;
    }
    
    public bool RaycastTransparentPlane(Vector3 screenPos, LayerMaskName layerMaskName, out Vector3 hitPos)
    {
        _dummyCam.gameObject.SetActive(true);
        
        Vector3 result = Vector3.zero;
        Ray ray = _dummyCam.ScreenPointToRay(screenPos);
        if (layerMaskDict.TryGetValue(layerMaskName, out LayerMask layerMask))
        {
            if (Physics.Raycast(ray, out RaycastHit hitInfo, 999999, layerMask))
            {
                result = hitInfo.point;
                hitPos = result;
                _dummyCam.gameObject.SetActive(false);
                return true;
            }
        }
        
        _dummyCam.gameObject.SetActive(false);
        hitPos = Vector3.zero;
        return false;
    }
}

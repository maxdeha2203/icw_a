using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Unitys build in LineRenderer component is too limited in its options to draw lines as necessary for polygons
 * This class handles drawing of all lines necessary for a polygon
 * 
 * If a new corner is added to the polygon, call the AddCorner Method
 * If a corner is dragged, call the AdjustCornerPosition method
 * 
 * Whenever a new line is drawn or a corner is dragged, the following happens:
 * --> Determine the start and end of the line
 * --> Calculate vector that connects those two points
 * --> Calculate 4 orthogonal vectors that lie in the plane of the map
 * --> Scale the 4 orthogonal vectors to the desired width of the line
 * --> Calculate 4 world position points from the start point, end point, and 4 orthogonal vectors
 * --> Create a simple mesh with the resulting 4 wold position points (2 triangles, 4 vertices)
*/

public class CustomLineRenderer : MonoBehaviour
{
    List<Vector3> polyCornerList;
    Vector3[] allVertices;
    int[] totalTriangles;
    Renderer renderer;
    MeshFilter mf;
    Mesh mesh;
    [SerializeField] float lineWidth;
    [SerializeField] Material lineMaterial;
    [SerializeField] Camera dummyCam;

    private void Awake()
    {
        polyCornerList = new List<Vector3>();
        renderer = GetComponent<Renderer>();
        mf = GetComponent<MeshFilter>();
        allVertices = new Vector3[0];
        totalTriangles = new int[0];
        mesh = new Mesh();
    }

    public void RescaleToMaintainScreenSize(float scaleFactor)
    {
        
    }
    public void SetInitialScale()
    {
        //gameObject.transform.localScale = initialScale * (1 / gameObject.transform.parent.transform.lossyScale.x);
    }

    public void AddCorner(Vector3 corner)
    {
        polyCornerList.Add(corner);
        OnPostCornerAdd();
    }

    public void CloseLoop()
    {
        DrawNewLine(polyCornerList[polyCornerList.Count - 1], polyCornerList[0]);
    }

    public void AdjustCornerPosition(int cornerIndex, bool isFinished, Vector3 deltaMove)
    {
        Vector3 vec = polyCornerList[cornerIndex];
        vec += deltaMove;
        polyCornerList[cornerIndex] = vec;
        Vector3[] adjustedVertices;

        if (cornerIndex == 0)
        {
            if (isFinished)
            {
                adjustedVertices = GetOrthogonalVectors(polyCornerList[polyCornerList.Count - 1], polyCornerList[0]);
                adjustedVertices.CopyTo(allVertices, allVertices.Length - 4);

                adjustedVertices = GetOrthogonalVectors(polyCornerList[0], polyCornerList[1]);
                adjustedVertices.CopyTo(allVertices, 0);
            }
            else
            {
                adjustedVertices = GetOrthogonalVectors(polyCornerList[0], polyCornerList[1]);
                adjustedVertices.CopyTo(allVertices, 0);
            }
        }
        else if (cornerIndex == polyCornerList.Count - 1)
        {
            if (isFinished)
            {
                adjustedVertices = GetOrthogonalVectors(polyCornerList[cornerIndex - 1], polyCornerList[cornerIndex]);
                adjustedVertices.CopyTo(allVertices, 4 * (cornerIndex - 1));

                adjustedVertices = GetOrthogonalVectors(polyCornerList[cornerIndex], polyCornerList[0]);
                adjustedVertices.CopyTo(allVertices, 4 * (cornerIndex));
            }
            else
            {
                adjustedVertices = GetOrthogonalVectors(polyCornerList[cornerIndex - 1], polyCornerList[cornerIndex]);
                adjustedVertices.CopyTo(allVertices, 4 * (cornerIndex - 1));
            }
        }
        else
        {
            adjustedVertices = GetOrthogonalVectors(polyCornerList[cornerIndex - 1], polyCornerList[cornerIndex]);
            adjustedVertices.CopyTo(allVertices, 4 * (cornerIndex - 1));

            adjustedVertices = GetOrthogonalVectors(polyCornerList[cornerIndex], polyCornerList[cornerIndex + 1]);
            adjustedVertices.CopyTo(allVertices, 4 * (cornerIndex));
        }
        mesh.vertices = allVertices;
    }

    void OnPostCornerAdd()
    {
        if (polyCornerList.Count > 1)
        {
            DrawNewLine(polyCornerList[polyCornerList.Count - 2], polyCornerList[polyCornerList.Count - 1]);
        }
    }

    void DrawNewLine(Vector3 start, Vector3 end)
    {
        Vector3[] newVerts = new Vector3[allVertices.Length + 4];
        allVertices.CopyTo(newVerts, 0);
        allVertices = newVerts;
        
        int[] newTris = new int[totalTriangles.Length + 6];
        totalTriangles.CopyTo(newTris, 0);
        totalTriangles = newTris;

        Vector3[] newNormals = new Vector3[allVertices.Length];

        int newVertCount = allVertices.Length;
        int newTrisCount = totalTriangles.Length;
        
        mf.mesh = mesh;

        Vector3 a = end - start;
        Vector3 b = (end - dummyCam.transform.position);
        b.Normalize();
        Vector3 cross = Vector3.Cross(a, b);
        cross.Normalize();
        cross *= lineWidth;

        allVertices[newVertCount - 4] = start - cross + b * 30;
        allVertices[newVertCount - 3] = start + cross + b * 30;
        allVertices[newVertCount - 2] = end - cross + b * 30;
        allVertices[newVertCount - 1] = end + cross + b * 30;

        mesh.vertices = allVertices;

        totalTriangles[newTrisCount - 6] = newVertCount - 4;
        totalTriangles[newTrisCount - 5] = newVertCount - 2;
        totalTriangles[newTrisCount - 4] = newVertCount - 3;

        newTris[newTrisCount - 3] = newVertCount - 3;
        newTris[newTrisCount - 2] = newVertCount - 2;
        newTris[newTrisCount - 1] = newVertCount - 1;

        mesh.triangles = totalTriangles;

        for (int i = 0; i < newNormals.Length; i++)
        {
            newNormals[i] = -Vector3.up;
        }
        mesh.normals = newNormals;

        renderer.material = lineMaterial;
    }

    public int GetPositionCount()
    {
        return polyCornerList.Count;
    }

    private Vector3[] GetOrthogonalVectors(Vector3 start, Vector3 end)
    {
        Vector3[] vecs = new Vector3[4];
        Vector3 a = end - start;
        Vector3 b = Vector3.forward;
        Vector3 cross = Vector3.Cross(a, b);
        cross.Normalize();
        cross *= lineWidth;
        vecs[0] = start - cross;
        vecs[1] = start + cross;
        vecs[2] = end - cross;
        vecs[3] = end + cross;
        return vecs;
    }
}

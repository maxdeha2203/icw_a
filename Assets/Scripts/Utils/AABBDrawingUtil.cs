using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class AABBDrawingUtil : MonoBehaviour
{
    RectTransform _panelRect;
    Vector2 _touchStart;
    Vector2 _touchEnd;

    float[] _p1 = new float[2];
    float[] _p2 = new float[2];

    public void SetPanelRect(GameObject newPanel)
    {
        _panelRect = newPanel.GetComponent<RectTransform>();
    }

    void Update()
    {
#if UNITY_ANDROID
        if (Input.touches.Length != 0 && !UiTestUtil.GetInstance().IsPointerOverUIElement(Input.touches[0].position))
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                _touchStart = touch.position;
            }
            _touchEnd = touch.position;
            AdjustRectPosition();
        }
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0) && !UiTestUtil.GetInstance().IsPointerOverUIElement(Input.mousePosition))
        {
            Vector3 touch = Input.mousePosition;
            _touchStart = new Vector2(touch.x, touch.y);
        }
        if (Input.GetMouseButton(0) && !UiTestUtil.GetInstance().IsPointerOverUIElement(Input.mousePosition))
        {
            Vector3 touch = Input.mousePosition;
            _touchEnd = new Vector2(touch.x, touch.y);
            AdjustRectPosition();
        }
#endif
    }

    void AdjustRectPosition()
    {
        if (
            !UiTestUtil.GetInstance().IsPointerOverUIElement(_touchStart)
            &&
            !UiTestUtil.GetInstance().IsPointerOverUIElement(_touchEnd)
        )
        {
            Vector2 sizeDelta = _touchEnd - _touchStart;
            float xPos = _touchStart.x;
            float yPos = _touchStart.y;

            if (sizeDelta.y < 0 | sizeDelta.x < 0)
            {
                if (sizeDelta.y < 0)
                {
                    yPos = _touchStart.y + sizeDelta.y;
                }
                if (sizeDelta.x < 0)
                {
                    xPos = _touchStart.x + sizeDelta.x;
                }
                sizeDelta.x = Math.Abs(sizeDelta.x);
                sizeDelta.y = Math.Abs(sizeDelta.y);
            }
            _panelRect.anchoredPosition = new Vector2(xPos, yPos);
            _panelRect.sizeDelta = sizeDelta;
        }
    }

    public void SetRectUvCoords()
    {
        float width = Screen.width;
        float height = Screen.height;
        Vector2 position = _panelRect.anchoredPosition;
        Vector2 sizeDelta = _panelRect.sizeDelta;
        _p1[0] = position.x / width;
        _p1[1] = position.y / height;
        _p2[0] = (position.x + sizeDelta.x) / width;
        _p2[1] = (position.y + sizeDelta.y) / height;
    }

    public float[,] GetUvRectCoords()
    {
        float[,] uvs = new float[2,2];
        uvs[0,0] = _p1[0];
        uvs[0,1] = _p1[1];
        uvs[1,0] = _p2[0];
        uvs[1,1] = _p2[1];
        return uvs;
    }
}

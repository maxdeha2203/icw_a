using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class PolyDrawingUtil : MonoBehaviour
{
    [SerializeField]
    GameObject cornerPrefab;
    
    [SerializeField] 
    GameObject polygonParent;

    [SerializeField]
    FarPlaneRoiTracker farPlaneRoiTracker;

    CustomLineRenderer clr;
    RectTransform _panelRect;
    Vector2 _touchStart;
    Vector2 _touchEnd;

    List<Vector3> cornerCoords = new List<Vector3>();
    List<Vector3> cornerCoordsLocalSpace = new List<Vector3>();

    float[] _p1 = new float[2];
    float[] _p2 = new float[2];

    void Start()
    {
        clr = GetComponent<CustomLineRenderer>();
    }

    public void SetPanelRect(GameObject newPanel)
    {
        _panelRect = newPanel.GetComponent<RectTransform>();
    }

    void Update()
    {
#if UNITY_ANDROID
        if (Input.touches.Length != 0 && !UiTestUtil.GetInstance().IsPointerOverUIElement(Input.touches[0].position))
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                _touchStart = touch.position;
            }
            _touchEnd = touch.position;
            AdjustRectPosition();
        }
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0) && !UiTestUtil.GetInstance().IsPointerOverUIElement(Input.mousePosition))
        {
            Vector3 touch = Input.mousePosition;
            if (TransparentPlaneRaycastingUtil.GetInstance().RaycastTransparentPlane(touch, LayerMaskName.PolygonCorners, out Vector3 result))
            {
                FinishPolygon();
            }
            else
            {
                CreateNewCorner(touch);
            }
        }
        if (Input.GetMouseButton(0) && !UiTestUtil.GetInstance().IsPointerOverUIElement(Input.mousePosition))
        {
            
        }
#endif
    }

    void FinishPolygon()
    {
        clr.CloseLoop();
        //create plane with AABB
        PolygonTrackingManager.GetInstance().SetPolygonProgress(PolygonProgressState.Finished);
        //create texture
        //set texture
    }

    void PointInPolygon()
    {
        
    }

    void CreateNewCorner(Vector3 touchPos)
    {
        if (TransparentPlaneRaycastingUtil.GetInstance().RaycastTransparentPlane(touchPos, LayerMaskName.TransparentPlane, out Vector3 hitPos))
        {
            GameObject plane = farPlaneRoiTracker.GetTransparentPlane();
            GameObject corner = Instantiate(cornerPrefab, hitPos, Quaternion.identity);
            corner.transform.parent = plane.transform.GetChild(0).transform;
            corner.transform.localScale = new Vector3(corner.transform.localScale.y, corner.transform.localScale.y, 0f);
            clr.AddCorner(hitPos);
            cornerCoords.Add(touchPos);
            cornerCoordsLocalSpace.Add(corner.transform.localPosition);
        }
    }

    void AdjustRectPosition()
    {   
        if (
            !UiTestUtil.GetInstance().IsPointerOverUIElement(_touchStart)
            &&
            !UiTestUtil.GetInstance().IsPointerOverUIElement(_touchEnd)
        )
        {
            Vector2 sizeDelta = _touchEnd - _touchStart;
            float xPos = _touchStart.x;
            float yPos = _touchStart.y;

            if (sizeDelta.y < 0 | sizeDelta.x < 0)
            {
                if (sizeDelta.y < 0)
                {
                    yPos = _touchStart.y + sizeDelta.y;
                }
                if (sizeDelta.x < 0)
                {
                    xPos = _touchStart.x + sizeDelta.x;
                }
                sizeDelta.x = Math.Abs(sizeDelta.x);
                sizeDelta.y = Math.Abs(sizeDelta.y);
            }
            _panelRect.anchoredPosition = new Vector2(xPos, yPos);
            _panelRect.sizeDelta = sizeDelta;
        }
    }

    public float[] GetAabbCoords()
    {
        float xMax;
        float xMin;
        float yMax;
        float yMin;
        xMin = cornerCoords[0].x;
        xMax = cornerCoords[0].x;
        yMin = cornerCoords[0].y;
        yMax = cornerCoords[0].y;
        foreach (var corner in cornerCoords)
        {
            xMin = Math.Min(xMin, corner.x);
            xMax = Math.Max(xMax, corner.x);
            yMin = Math.Min(yMin, corner.y);
            yMax = Math.Max(yMax, corner.y);
        }

        return new float[] { xMin, yMin, xMax, yMax };
    }

    public List<Vector3> GetCornerCoords()
    {
        return cornerCoords;
    }
    
    public List<Vector3> GetCornerCoordsLocalSpace()
    {
        return cornerCoordsLocalSpace;
    }

    public List<Vector2> GetCornerUvCoords(float[] aabbCoords)
    {
        List<Vector2> uvCoordList = new List<Vector2>();
        for (int i = 0; i < cornerCoords.Count; i++)
        {
            float width = aabbCoords[2] - aabbCoords[0];
            float height = aabbCoords[3] - aabbCoords[1];
            float u = (cornerCoords[i].x - aabbCoords[0]) / width;
            float v = (cornerCoords[i].y - aabbCoords[1]) / height;
            uvCoordList.Add(new Vector2(u, v));
        }
        return uvCoordList;
    }
}

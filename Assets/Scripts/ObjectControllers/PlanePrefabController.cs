using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanePrefabController : MonoBehaviour
{
    public void SetPlaneTexture(Texture2D texture2d)
    {
        MeshRenderer mr = GetComponentInChildren<MeshRenderer>();
        mr.material.mainTexture = texture2d;
    }
}

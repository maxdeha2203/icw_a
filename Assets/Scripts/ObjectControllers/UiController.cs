using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiController : MonoBehaviour
{
    static UiController _instance;
    
    // Start is called before the first frame update
    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;
    }

    public static UiController GetInstance()
    {
        return _instance;
    }

    public void SetUiVisibility(bool visibility)
    {
        gameObject.SetActive(visibility);
    }
}

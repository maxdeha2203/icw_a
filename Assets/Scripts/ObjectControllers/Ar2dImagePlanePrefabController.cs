using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ar2dImagePlanePrefabController : MonoBehaviour
{
    public void SetPlaneTexture(Texture2D texture2d)
    {
        MeshRenderer mr = GetComponentInChildren<MeshRenderer>();
        mr.material.mainTexture = texture2d;
    }
}

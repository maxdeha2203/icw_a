using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugTextController : MonoBehaviour
{
    static DebugTextController _instance;

    string _displayText = "";

    TMP_Text tmp;
    
    // Start is called before the first frame update
    void Start()
    {
        if (_instance != null && _instance != this) Destroy(this);
        else _instance = this;

        tmp = GetComponent<TMP_Text>();
    }

    public static DebugTextController GetInstance()
    {
        return _instance;
    }

    public void SetDisplayText(string text)
    {
        _displayText = text;
        tmp.SetText(_displayText);
    }
}

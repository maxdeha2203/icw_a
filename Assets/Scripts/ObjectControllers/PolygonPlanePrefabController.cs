using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.InputSystem;
using UnityEngine;

public class PolygonPlanePrefabController : MonoBehaviour
{
    public void CreateMesh(Vector3[] cornerCoords, Vector2[] uvs)
    {
        var mesh = new Mesh();
        
        mesh.vertices = cornerCoords;

        Vector2[] cornerCoords2d = new Vector2[cornerCoords.Length];
        for (int i = 0; i < cornerCoords.Length; i++)
        {
            cornerCoords2d[i] = new Vector2(cornerCoords[i].x, cornerCoords[i].y);
        }
        Triangulator triangulator = new Triangulator(cornerCoords2d);
        int[] tris = triangulator.Triangulate();
        mesh.triangles = tris;

        mesh.uv = uvs;
        
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        GetComponent<MeshFilter>().mesh = mesh;

        Vector4 barycentricCoords = Vector4.zero;
        for (int i = 0; i < uvs.Length; i++)
        {
            barycentricCoords.x += uvs[i][0];
            barycentricCoords.y += uvs[i][1];
        }
        barycentricCoords /= uvs.Length;
        Debug.Log(barycentricCoords);
        GetComponent<MeshRenderer>().material.SetVector("_BarycentricCoordinates", barycentricCoords);
    }
}
